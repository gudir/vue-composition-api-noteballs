import { defineStore } from "pinia"

export const useStoreNotes = defineStore('storeNotes', {
    state: () => {
        return {
            notes: [
                {
                    id: 'id1',
                    content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro quisquam dolores possimus dolorum! Id reprehenderit non enim vitae ut repudiandae temporibus, assumenda voluptates incidunt est excepturi culpa laudantium eius! Repellendus!'
                },
                {
                    id: 'id2',
                    content: 'This is a short note! woo'
                }
            ]
        }
    },
    actions: {
        addNote(newNote) {
            let currentDateTime = new Date().getTime();
            let id = currentDateTime.toString();

            let note = {
                id,
                content: newNote
            }

            this.notes.unshift(note)
        },
        deleteNote(idNoteToDelete) {
            this.notes = this.notes.filter(note => note.id !== idNoteToDelete);
        },
        updateNote(payload) {
            let indexNote = this.notes.findIndex(note => note.id === payload.id)
            this.notes[indexNote].content = payload.content
        }
    },
    getters: {
        getNoteContent: (state) => {
            return (id) => {
                return state.notes.find(note => note.id === id).content;
            } 
        },
        totalNoteCount: (state) => {
            return state.notes.length;
        },
        totalCharactersCount: (state) => {
            return state.notes.map(note => note.content.length).reduce((valueLengthPrev, valueLength) => {
                return valueLengthPrev + valueLength
            })
        }
    }
})