import { createApp } from 'vue';
import { createPinia } from 'pinia'
import App from './App.vue';
import router from './routes';
import 'bulma/css/bulma.min.css';

createApp(App)
    .use(createPinia())
    .use(router)
    .mount('#app')
